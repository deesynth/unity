﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{

    //Time parameters
    [SerializeField] private Text timeText;
    private string defaultTime = "Time: 0.0 s";
    private float movingTime;

    public float timeInSeconds;

    //Rectilinear movement parameters
    public Vector3 rectilinearStartPosition;
    public Vector3 rectilinearFinishPosition;

    //Zigzag movement parameters
    public Vector3 zigzagStartPosition;
    public Vector3 zigzagFinishPosition;
    public int zigzagN;
    public float zigzagHeight;

    //Spiral movement parameters
    public int spiralN;
    public float spiralRadius;
    public Vector3 pointO;
    private float angle;
    private float deltaSpiralRadius;
    private float newSpiralRadius;
    private float distanceCircle;

    //Variables for calculation
    private float rectilinearSpeed;
    private float zigzagSpeed;
    private float spiralSpeed;

    private Vector3 topPosition;

    private float distanceIncrement;
    private int i;
    private int j;


    //Button enumeration
    enum Button
    {
        Empty,
        Rectilinear,
        Zigzag,
        Spiral
    }

    Button btn;

    // Start is called before the first frame update
    void Start()
    {
        timeText.text = defaultTime;
        movingTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Menu
        switch(btn)
        {
            case Button.Rectilinear:
                RectilinearMovement();
                break;
            case Button.Zigzag:
                ZigzagMovement();
                break;
            case Button.Spiral:
                SpiralMovement();
                break;
            default:
                EmptyMovement();
                break;
        }
    }

    public void RectilinearMovement()
    {
        if (transform.position != rectilinearFinishPosition)
        {
            ShowTime();
        }

        transform.position = Vector3.MoveTowards(transform.position, rectilinearFinishPosition, rectilinearSpeed * Time.deltaTime);
    }

    public void ZigzagMovement()
    {

        if (transform.position != topPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, topPosition, zigzagSpeed * Time.deltaTime);
            ShowTime();
        }
        else if (i < zigzagN * 2)
        {
            topPosition.x += distanceIncrement;

            if (i % 2 == 0)
            {
                topPosition.y += zigzagHeight;
            }
            else
            {
                topPosition.y -= zigzagHeight;
            }

            i++;
        }
    }

    public void SpiralMovement()
    {
        /////SpiralMovement calculation
        
        if(angle >= 2 * Mathf.PI)
        {
            angle = 0;
        }

        angle += 2 * Mathf.PI * Time.deltaTime / ((float)timeInSeconds / spiralN);
        newSpiralRadius -= spiralRadius / spiralN * Time.deltaTime / ((float)timeInSeconds / spiralN);
        Debug.Log("angle = " + angle);
        //Debug.Log("newSpiralRadius = " + newSpiralRadius);

        if (newSpiralRadius > 0)
        {
            var x = -Mathf.Cos(angle) * newSpiralRadius;
            var y = Mathf.Sin(angle) * newSpiralRadius;

            if (transform.position != pointO)
            {
                transform.position = new Vector3(x, y, 0) + pointO;
                ShowTime();
            }
        }

    }

    public void EmptyMovement()
    {
        transform.position = Vector3.zero;
    }

    public void RectilinearButtonPressed()
    {
        btn = Button.Rectilinear;
        RectilinearStartConditions();
    }

    public void ZigzagButtonPressed()
    {
        btn = Button.Zigzag;
        ZigzagStartConditions();
    }

    public void SpiralButtonPressed()
    {
        btn = Button.Spiral;
        SpiralStartConditions();
    }

    public void RectilinearStartConditions()
    {
        transform.position = rectilinearStartPosition;
        movingTime = 0;

        /////RectilinearMovement calculation

        transform.position = rectilinearStartPosition;

        var heading = rectilinearStartPosition - rectilinearFinishPosition;
        var distance = heading.magnitude;

        rectilinearSpeed = distance / timeInSeconds;
    }

    public void ZigzagStartConditions()
    {
        transform.position = zigzagStartPosition;
        movingTime = 0;

        /////ZigzagMovement calculation
        
        var heading = zigzagStartPosition - zigzagFinishPosition;
        var distance = heading.magnitude;

        topPosition = zigzagStartPosition;
        distanceIncrement = distance / zigzagN / 2;

        Vector3 topPos1 = new Vector3(zigzagStartPosition.x + distanceIncrement, zigzagStartPosition.y + zigzagHeight, zigzagStartPosition.z);
        var headingTop = zigzagStartPosition - topPos1;
        var distanceTop = headingTop.magnitude;
        zigzagSpeed = distanceTop * zigzagN * 2 / timeInSeconds;

        i = 0;
    }

    public void SpiralStartConditions()
    {
        transform.position = new Vector3(pointO.x, pointO.y + spiralRadius, pointO.z);
        Debug.Log("start pos: " + transform.position.x + " " + transform.position.y + " " + transform.position.z);
        movingTime = 0;
        newSpiralRadius = spiralRadius;
        angle = Mathf.PI/2;
    }

    public void ShowTime()
    {
        movingTime += Time.deltaTime;
        timeText.text = "Time: " + movingTime + " s";
    }


    public void QuitButtonPressed()
    {
        Application.Quit();
    }



}
