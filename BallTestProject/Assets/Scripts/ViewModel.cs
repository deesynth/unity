﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewModel : MonoBehaviour
{
    [SerializeField] private Ball ball;
    //private Ball ball;

    // Start is called before the first frame update
    void Start()
    {
        //ball = GetComponent<Ball>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TimeEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.timeInSeconds = temp;
    }

    public void RectilinearStartPositionXEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.rectilinearStartPosition.x = temp;
    }

    public void RectilinearStartPositionYEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.rectilinearStartPosition.y = temp;
        
    }

    public void RectilinearFinishPositionXEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.rectilinearFinishPosition.x = temp;
    }

    public void RectilinearFinishPositionYEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.rectilinearFinishPosition.y = temp;

    }

    public void ZigzagStartPositionXEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.zigzagStartPosition.x = temp;
    }

    public void ZigzagStartPositionYEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.zigzagStartPosition.y = temp;
    }

    public void ZigzagFinishPositionXEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.zigzagFinishPosition.x = temp;
    }

    public void ZigzagFinishPositionYEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.zigzagFinishPosition.y = temp;
    }

    public void ZigzagNEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        int temp = int.Parse(newText);
        ball.zigzagN = temp;
    }

    public void ZigzagHeightEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.zigzagHeight = temp;
    }

    public void SpiralOXEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.pointO.x = temp;
    }

    public void SpiralOYEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.pointO.y = temp;
    }

    public void SpiralRadiusEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        float temp = float.Parse(newText);
        ball.spiralRadius = temp;
    }

    public void SpiralNEnd(string newText)
    {
        Debug.Log("tempEnd = " + newText);
        int temp = int.Parse(newText);
        ball.spiralN = temp;
    }
}
